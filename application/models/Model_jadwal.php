<?php
Class Model_jadwal extends CI_Model
{ 
    private $_table = 'tb_jadwal';

    public function rules()
    {
        return [
            ['field' => 'tgl_berangkat',
            'label' => 'Tanggal berangkat',
            'rules' => 'required'],

            ['field' => 'tgl_sampai',
            'label' => 'Tanggal sampai',
            'rules' => 'required'],

            ['field' => 'asal',
            'label' => 'Asal keberangkatan',
            'rules' => 'required'],

            ['field' => 'tujuan',
            'label' => 'Tujuan',
            'rules' => 'required'],

            ['field' => 'ongkos',
            'label' => 'Ongkos',
            'rules' => 'required'],

            ['field' => 'mobil',
            'label' => 'Mobil',
            'rules' => 'required'],

            ['field' => 'kategori',
            'label' => 'Kategori',
            'rules' => 'required'],
        
        ];
    }

    public function jumlahJadwal()
    {
        return $this->db->count_all('tb_jadwal');
    }

    public function tampil($id = null)
    {
        $this->db->select('tb_jadwal.*, Asal.nama_perjalanan AS ASAL, Tujuan.nama_perjalanan AS TUJUAN, Kategori.nama_kategori AS KATEGORI, Mobil.nama_mobil AS MOBIL');
        $this->db->from('tb_jadwal');
        $this->db->join('tb_perjalanan as Asal','tb_jadwal.asal = Asal.id_perjalanan', 'left');
        $this->db->join('tb_perjalanan as Tujuan','tb_jadwal.tujuan = Tujuan.id_perjalanan', 'left');
        $this->db->join('tb_kategori as Kategori','tb_jadwal.kategori = Kategori.id_kategori', 'left');
        $this->db->join('tb_mobil as Mobil','tb_jadwal.mobil = Mobil.id_mobil', 'left');
        // $this->db->select('tb_mobil, id_mobil.mobil');
        return $this->db->get();
    }

    public function insert_jadwal()
    {
        $post = $this->input->post();
        $this->mobil = $post["mobil"];
        $this->asal = $post["asal"];
        $this->tujuan = $post["tujuan"];
        $this->tgl_berangkat = $post["tgl_berangkat"];
        $this->tgl_sampai = $post["tgl_sampai"];
        $this->ongkos = $post["ongkos"];
        $this->kategori = $post["kategori"];
        $this->tgl_dibuat = date('Y-m-d');

        $this->db->insert($this->_table, $this);
    }

    public function hapus_jadwal($table, $where)
    {
        return $this->db->delete($table, $where);
    }


}